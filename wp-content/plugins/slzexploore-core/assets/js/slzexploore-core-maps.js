(function ($) {
    "use strict";

    $.slzexploore_core_map_mainFunction = function () {
        /*Google map*/


        /* Begin Map contact short code*/
        var data = {};
        var timeout_map = 1;
        var attr = '';
        var block_class = '';
        $(".slz-shortcode .map-block").each(function () {
            var that = this;
            var googleMap = $(that).find("#googleMap");
            var myLatLng = {};
            var markerLatLng = {};
            var markerTitle = '';
            attr = $(this).attr('data-id');
            var map_api = $(this).attr('data-map-api');
            if (attr != '') {
                block_class = '.contact-' + attr + ' ';
            }
            if ($(block_class + "#googleMap").length) {
                timeout_map = 1000;
                var address = $(block_class + '#googleMap').data('address');
                if (address != '') {
                    $.ajax({
                        url: "https://maps.googleapis.com/maps/api/geocode/json?address=" + address + "&sensor=false&key="+map_api,
                        type: "POST",
                        success: function (res) {
                            if (res.status == 'OK') {
                                console.log(res);
                                markerTitle = address;
                                data.lat = res.results[0].geometry.location.lat;
                                data.lng = res.results[0].geometry.location.lng;
                                myLatLng = {lat: data.lat - 0.0022, lng: data.lng - 0.0055};
                                markerLatLng = {lat: parseFloat(data.lat), lng: parseFloat(data.lng)};
                            } else if (res.status == "OVER_QUERY_LIMIT") {
                                console.log(res.error_message);
                                googleMap.append('<p>' + res.error_message + '</p>');
                            }
                        }
                    });
                }
            }
            setTimeout(function () {


                var customMapType = new google.maps.StyledMapType(
                    [
                        {
                            "featureType": "water",
                            "stylers": [
                                {"color": "#f0f3f6"}
                            ]
                        },
                        {
                            "featureType": "road.highway",
                            "elementType": "geometry",
                            "stylers": [
                                {"color": "#adb3b7"}
                            ]
                        },
                        {
                            "featureType": "road.highway",
                            "elementType": "labels.icon",
                            "stylers": [
                                {"hue": "#ededed"}
                            ]
                        },
                        {
                            "featureType": "road.arterial",
                            "stylers": [
                                {"color": "#c8cccf"}
                            ]
                        },
                        {
                            "featureType": "road.local",
                            "stylers": [
                                {"color": "#e6e6e6"}
                            ]
                        },
                        {
                            "featureType": "landscape",
                            "stylers": [
                                {"color": "#ffffff"}
                            ]
                        },
                        {
                            "elementType": "labels.text",
                            "stylers": [
                                {"weight": 0.1},
                                {"color": "#6d6d71"}
                            ]
                        }
                    ],
                    {
                        name: 'Custom Style'
                    });
                var customMapTypeId = 'custom_style';

                var mapProp = {
                    center: myLatLng,
                    zoom: 16,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    scrollwheel: false,
                    draggable: false,
                    disableDefaultUI: true,
                    mapTypeControlOptions: {
                        mapTypeIds: [google.maps.MapTypeId.ROADMAP, customMapTypeId]
                    }
                };

                function initialize() {
                    if (data.lat) {
                        console.log('create ggmap');
                        var map = new google.maps.Map(googleMap[0], mapProp);
                        var image_url = googleMap.attr('data-img-url');
                        map.mapTypes.set(customMapTypeId, customMapType);
                        map.setMapTypeId(customMapTypeId);
                        var image = {
                            url: image_url,
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(17, 34),
                            scaledSize: new google.maps.Size(40, 40)
                        };
                        var marker = new google.maps.Marker({
                            position: markerLatLng,
                            map: map,
                            animation: google.maps.Animation.BOUNCE,
                            icon: image,
                            title: markerTitle
                        });

                        marker.addListener('click', function () {
                            $(that).find('.btn-open-map').parents('.map-info').toggle(400);
                        });
                    }
                }

                initialize();

                /*google.maps.event.addDomListener(window, 'load', initialize);*/


                $(that).find('.btn-open-map').click(function (event) {
                    /* Act on the event */
                    $(this).parents('.map-info').toggle(400);
                    googleMap.css('pointer-events', 'auto');
                    if ($(window).width() > 462) {
                        mapProp = {
                            center: markerLatLng,
                            zoom: 16,
                            mapTypeId: google.maps.MapTypeId.ROADMAP,
                            scrollwheel: false,
                            disableDefaultUI: true,
                            mapTypeControlOptions: {
                                mapTypeIds: [google.maps.MapTypeId.ROADMAP, customMapTypeId]
                            }
                        };
                    }
                    else {
                        mapProp = {
                            center: markerLatLng,
                            zoom: 15,
                            mapTypeId: google.maps.MapTypeId.ROADMAP,
                            scrollwheel: false,
                            navigationControl: false,
                            mapTypeControl: false,
                            mapTypeControlOptions: {
                                mapTypeIds: [google.maps.MapTypeId.ROADMAP, customMapTypeId]
                            }
                        };
                    }

                    initialize();
                });

            }, timeout_map);

        });
        /* End Map contact short code*/


    };

})(jQuery);

jQuery(document).ready(function () {
    if (jQuery("#googleMap").length) {
        jQuery.slzexploore_core_map_mainFunction();
    }
});
