<div class="wrap about-wrap slz-wrap slz-tab-style">
	<?php do_action( 'slzexploore_get_theme_header' ); ?>
    <div class="slz-demo-themes slz-install-plugins slz-history">
        <dl class="slz-changelog-list">
                        <dd>
            <h3>Version 5.7 ( 19 June 2019 )</h3>
                <div class="des">
                    <div class="update-content">
                        <p class="title">
                            <strong>Fix bug and update slzexploore-core plugin and theme.</strong>
                        </p>
                        <ul>
                            <li>Update plugin WPBakery Page Builder 6.0.3</li>
                            <li>Update plugin Revolution Slider 5.4.8.2</li>
                        </ul>
                    </div>
                </div>
            </dd>
        	            <dd>
 			<h3>Version 5.6 ( 06 March 2019 )</h3>
                <div class="des">
                    <div class="update-content">
                        <p class="title">
							<strong>Fix bug and update slzexploore-core plugin and theme.</strong>
                        </p>
                        <ul>
                            <li>Update plugin WPBakery Page Builder 5.7</li>
                            <li>Update plugin Revolution Slider 5.4.8.2</li>
                            <li>Update translations</li>
                            <li>Fix CSS issues</li>
                        </ul>
                    </div>
                </div>
            </dd>
            <dd>   
        	    <h3>Version 5.5 ( 04 January 2019 )</h3>
                <div class="des">
                    <div class="update-content">
                        <p class="title">
							<strong>Fix bug and update slzexploore-core plugin and theme.</strong>
                        </p>
                        <ul>
                            <li>Update plugin Revolution Slider 5.4.8.1</li>
                            <li>Improve CSS for booking form in mobile view </li>
                            <li>Fix CSS issues</li>
                        </ul>
                    </div>
                </div>
            </dd>
            <dd>       	
				<h3>Version 5.4 ( 12 December 2018 )</h3>
                <div class="des">
                    <div class="update-content">
                        <p class="title">
							<strong>Fix bug and update slzexploore-core plugin and theme.</strong>
                        </p>
                        <ul>
                            <li>Update plugin WPBakery Page Builder 5.6 (supports Wordpress 5.0)</li>
                            <li>Fix CSS for skin colors </li>
                            <li>Fix mobile-submenu scroll and CSS</li>
                        </ul>
                    </div>
                </div>
            </dd>
            <dd>
 				<h3>Version 5.3 ( 8 November 2018 )</h3>
                <div class="des">
                    <div class="update-content">
                        <p class="title">
                            <strong>Fix bug and update slzexploore-core plugin and theme.</strong>
                        </p>
                        <ul>
                            <li>Fix Google API key issue with showing map locations</li>
                        </ul>
                    </div>
                </div>
            </dd>
            <dd>
        	    <h3>Version 5.2 ( 18 October 2018 )</h3>
                <div class="des">
                    <div class="update-content">
                        <p class="title">
                            <strong>Fix bug and update slzexploore-core plugin and theme.</strong>
                        </p>
                        <ul>
                            <li>Update plugin WPBakery Page Builder Version 5.5.5</li>
                        </ul>
                    </div>
                </div>
            </dd>
            <dd>
                <h3>Version 5.1 ( 24 August 2018 )</h3>
                <div class="des">
                    <div class="update-content">
                        <p class="title">
                            <strong>Fix bug and update slzexploore-core plugin and theme.</strong>
                        </p>
                        <ul>
                            <li>Update CSS.</li>
                            <li>Update plugin WPBakery Page Builder Version 5.5.2</li>
                            <li>Update plugin Slider Revolution Version 5.4.8 StarPath.</li>
                            <li> Fix gallery in footer options.</li>
                        </ul>
                    </div>
                </div>
            </dd>
            <dd>
                <h3>Version 5.0 ( 3 August 2018 )</h3>
                <div class="des">
                    <div class="update-content">
                        <p class="title">
                            <strong>Fix bug and update slzexploore-core plugin and theme.</strong>
                        </p>
                        <ul>
                            <li>Update CSS.</li>
                            <li>Update template Woocommerce compability with version 3.4.0</li>
                            <li>Display Extra Item in cart and mail.</li>
                            <li> Fix duplicate send mail of Contact Form.</li>
                            <li> Fix Like button in Tour carousel.</li>
                        </ul>
                    </div>
                </div>
            </dd>
            <dd>
                <h3>Version 4.9 ( 31 May 2018 )</h3>
                <div class="des">
                    <div class="update-content">
                        <p class="title">
                            <strong>Fix bug and update slzexploore-core plugin and theme.</strong>
                        </p>
                        <ul>
                            <li>Update CSS.</li>
                            <li>Fix menu mobile.</li>
                            <li>Fix Datetime-Picker of Booking Search.</li>
                        </ul>
                    </div>
                </div>
            </dd>

            <dd>
                <h3>Version 4.8 ( 19 April 2018 )</h3>
                <div class="des">
                    <div class="update-content">
                        <p class="title">
                            <strong>Fix bug and update slzexploore-core plugin and theme.</strong>
                        </p>
                        <ul>
                            <li>Fixed: Default setting button.</li>
                            <li>Fixed: Menu on mobile.</li>
                            <li>Fixed: Cart template woocommerce.</li>
                            <li>Fixed: Polylang Flag.</li>
                            <li>Fixed: Typing error.</li>
                        </ul>
                    </div>
                </div>
            </dd>

            <dd>
                <h3>Version 4.7 ( 29 Mar 2018 )</h3>
                <div class="des">
                    <div class="update-content">
                        <p class="title">
                            <strong>Fix bug and update slzexploore-core plugin and theme.</strong>
                        </p>
                        <ul>
                            <li>Updated: Woocommerce template 3.3.4</li>
                            <li>Updated: CSS search shortcode</li>
                            <li>Fixed: CSS slider on mobile.</li>
                            <li>Fixed: Pagination search page.</li>
                        </ul>
                    </div>
                </div>
            </dd>
            <h3>Version 4.6 (26 Mar 2018 )</h3>
            <div class="des">
                <div class="update-content">
                    <p class="title">
                        <strong>Features - Update slzexploore-core plugin and theme.</strong>
                    </p>
                    <ul>
                        <li>Updated: Visual Composer 5.4.7</li>
                        <li>Updated: Revolution Slider 5.4.7.2</li>
                        <li>Fixed: Category location accomodation.</li>
                        <li>Fixed: Link void(0) negate SEO.</li>
                        <li>Updated: Add feature menu float fixed.</li>

                    </ul>
                </div>
            </div>
            </dd>
            <dd>
                <h3>Version 4.5 (11 January 2017 )</h3>
                <div class="des">
                    <div class="update-content">
                        <p class="title">
                            <strong>Features - Update slzexploore-core plugin and theme.</strong>
                        </p>
                        <ul>
                            <li>Updated: Revolution Slider 5.4.6.3.1</li>
                            <li>Fixed: display sub-categories SLZ Tour Category</li>
                            <li>Fixed: insert lightbox for Tour Description</li>
                            <li>Updated: login and register with Social Network</li>
                            <li>Updated: enter custom skin color on Theme Options</li>
                            <li>Updated: enter text with Tour Free.</li>
                            <li>Improved: pricing booking tour schedule</li>
                            <li>Improved: discount pricing</li>
                        </ul>
                    </div>
                </div>
            </dd>
            <dd>
                <h3>Version 4.4 (29 December 2017 )</h3>
                <div class="des">
                    <div class="update-content">
                        <p class="title">
                            <strong>Features - Update slzexploore-core plugin and theme.</strong>
                        </p>
                        <ul>
                            <li>Fix: bug on Page Setting.</li>
                        </ul>
                    </div>
                </div>
            </dd>
            <dd>
                <h3>Version 4.3 (26 December 2017 )</h3>
                <div class="des">
                    <div class="update-content">
                        <p class="title">
                            <strong>Features - Update slzexploore-core plugin and theme.</strong>
                        </p>
                        <ul>
                            <li>Update: Compatible with Wordpress 4.9.1</li>
                            <li>Update: update Visual Composer 4.5.4</li>
                        </ul>
                    </div>
                </div>
            </dd>
            <dd>
                <h3>Version 4.2 (29 September 2017)</h3>
                <div class="des">
                    <div class="update-content">
                        <p class="title">
                            <strong>Features - Update slzexploore-core plugin.</strong>
                        </p>
                        <ul>
                            <li>Fixed slider on Page Setting</li>
                        </ul>
                    </div>
                </div>
            </dd>
            <dd>
                <h3>Version 4.1 (24 August 2017)</h3>
                <div class="des">
                    <div class="update-content">
                        <p class="title">
                            <strong>Features - Update slzexploore-core plugin and theme.</strong>
                        </p>
                        <ul>
                            <li>Updated: Visual Composer Builder version 5.2.1</li>
                            <li>Updated: Slider Revolution version 5.4.5.1</li>
                            <li>Fixed: SLZ Tour Category: Show stucture categories.</li>
                            <li>Fixed: Display tours on Tour tag detail.</li>
                        </ul>
                    </div>
                </div>
            </dd>
            <dd>
                <h3>Version 4.0 (23 June 2017)</h3>
                <div class="des">
                    <div class="update-content">
                        <p class="title">
                            <strong>Features - Update slzexploore-core plugin and theme.</strong>
                        </p>
                        <ul>
                            <li>Added: Text Block in Header Top.</li>
                            <li>Added: Accommodation, Tour, Car, Cruise post can use Slider Revolution or Customer
                                Slider as Header Content. (Page Setting > Header Content.)
                            </li>
                            <li>Updated: Allow cruise booking without choose cabin type.</li>
                            <li>Updated: Extra Items can support by Hotel, Tour, Car, Cruise posts.</li>
                            <li>Updated: Shortcode SLZ Tour Schedule can add multiple images (Slick Slider).</li>
                            <li>Updated: Page Title Background for Post Type Hotel, Tour, Car, Cruise (Setting in Theme
                                Options: Accommodations Setting, Tour Setting, Car Rent Setting, Cruises Setting ).
                            </li>
                            <li>Updated: Shortcode Tour Grid and Cruise Grid style 1 column.</li>
                            <li>Fixed: Show page title bug.</li>
                            <li>Fixed: Contact Form 7 submit twice bug.</li>
                            <li>Fixed: CSS bugs.</li>
                        </ul>
                    </div>
                </div>
            </dd>
            <dd>
                <h3>Version 3.10 (13 June 2017)</h3>
                <div class="des">
                    <div class="update-content">
                        <p class="title">
                            <strong>Features - Update slzexploore-core plugin and theme.</strong>
                        </p>
                        <ul>
                            <li>Updated: Slider Revolution version 5.4.5.1</li>
                            <li>Fixed: Bugs in share link and theme options.</li>
                        </ul>
                    </div>
                </div>
            </dd>
            <dd>
                <h3>Version 3.9 (26 May 2017)</h3>
                <div class="des">
                    <div class="update-content">
                        <p class="title">
                            <strong>Features - Update slzexploore-core plugin and theme.</strong>
                        </p>
                        <ul>
                            <li>Added: Setting to automatically hide tours which have already passed.</li>
                            <li>Fixed: Save location in the map when edit tour, car or Cruise in admin.</li>
                            <li>Updated: Mail template for services( Hotel, Tour, Car, Cruise ) when booking.</li>
                            <li>Updated: pot file.</li>
                            <li>Updated: SLZ Recent New shortcode - Hide image in front-end when the post have not
                                feature image.
                            </li>
                            <li>Updated: Slzexploore Core version 3.9</li>
                        </ul>
                    </div>
                </div>
            </dd>
            <dd>
                <h3>Version 3.8 (17 April 2017)</h3>
                <div class="des">
                    <div class="update-content">
                        <p class="title">
                            <strong>Features - Update slzexploore-core plugin and theme.</strong>
                        </p>
                        <ul>
                            <li>Fixed: Hide variable products in shop page when updating to the WooCommerce version
                                3.0.0 and later.
                            </li>
                        </ul>
                    </div>
                </div>
            </dd>
            <dd>
                <h3>Version 3.7 (14 April 2017)</h3>
                <div class="des">
                    <div class="update-content">
                        <p class="title">
                            <strong>Features - Update slzexploore-core plugin and theme.</strong>
                        </p>
                        <ul>
                            <li>Updated: Booking feature to compatible with the WooCommerce version 3.0.0 and later.
                            </li>
                        </ul>
                    </div>
                </div>
            </dd>
            <dd>
                <h3>Version 3.6 (10 April 2017)</h3>
                <div class="des">
                    <div class="update-content">
                        <p class="title">
                            <strong>Features - Update slzexploore-core plugin and theme.</strong>
                        </p>
                        <ul>
                            <li>Improved: Localized numeric and monetary formatting information.</li>
                            <li>Updated: Visual Composer ver 5.1.1.</li>
                        </ul>
                    </div>
                </div>
            </dd>
            <dd>
                <h3>Version 3.5 (15 March 2017)</h3>
                <div class="des">
                    <div class="update-content">
                        <p class="title">
                            <strong>Features - Update slzexploore-core plugin and theme.</strong>
                        </p>
                        <ul>
                            <li>Added: Menu topbar in page option.</li>
                            <li>Added: Display number of star in hotel detail page.</li>
                            <li>Improved: SLZ Hotel Grid shortcode - Add three column option.</li>
                            <li>Improved: SLZ Image Gallery shortcode - Add link for Image Regions style.</li>
                            <li>Improved: SLZ Map Location shortcode - Display image on map and enable sort option.</li>
                            <li>Improved: Hotel search.</li>
                            <li>Updated: pot file.</li>
                            <li>Updated: Visual Composer ver 5.1.</li>
                        </ul>
                    </div>
                </div>
            </dd>
            <dd>
                <h3>Version 3.4 (10 March 2017)</h3>
                <div class="des">
                    <div class="update-content">
                        <p class="title">
                            <strong>Features - Update slzexploore-core plugin and theme.</strong>
                        </p>
                        <ul>
                            <li>Added: Set different tour prices with different dates.</li>
                            <li>Added: Set different tour available seats with different dates.</li>
                            <li>Added: Set rooms of hotel are not show in frontend.</li>
                            <li>Improved: Create product categories corresponding to services( Hotel, Tour, Car, Cruise
                                ) when booking, support for reporting.
                            </li>
                            <li>Improved: Tour search.</li>
                            <li>Updated: pot file.</li>
                            <li>Updated: Slider Revolution version 5.4.1</li>
                        </ul>
                    </div>
                </div>
            </dd>
            <dd>
                <h3>Version 3.3 (07 March 2017)</h3>
                <div class="des">
                    <div class="update-content">
                        <p class="title">
                            <strong>Features - Update slzexploore-core plugin and theme.</strong>
                        </p>
                        <ul>
                            <li>Added: Display menu in header topbar.</li>
                            <li>Added: Configure show or hide author box in blog page.</li>
                            <li>Improved: Display price with decimal number.</li>
                            <li>Improved: Tour Category and Item List shortcode.</li>
                            <li>Updated: pot file.</li>
                        </ul>
                    </div>
                </div>
            </dd>
            <dd>
                <h3>Version 3.2 (16 December 2016)</h3>
                <div class="des">
                    <div class="update-content">
                        <p class="title">
                            <strong>Features - Update slzexploore-core plugin and theme.</strong>
                        </p>
                        <ul>
                            <li>Improved: Setting booking method - show both booking form and contact form.</li>
                            <li>Updated: Slider Revolution version 5.3.1.5.</li>
                            <li>Fixed: Accommodation Booking.</li>
                            <li>Fixed: Responsive bugs</li>
                        </ul>
                    </div>
                </div>
            </dd>
            <dd>
                <h3>Version 3.1 (09 December 2016)</h3>
                <div class="des">
                    <div class="update-content">
                        <p class="title">
                            <strong>Features - Update slzexploore-core plugin and theme.</strong>
                        </p>
                        <ul>
                            <li>Added: Set different prices for different dates of accommodation</li>
                            <li>Improved: Booking form - Disable unavailable date in calendar.</li>
                            <li>Updated: Slider Revolution version 5.3.1.</li>
                            <li>Fixed: Bug when active slzexploore-core plugin.</li>
                            <li>Fixed: Responsive bugs</li>
                        </ul>
                    </div>
                </div>
            </dd>
            <dd>
                <h3>Version 3.0.1 (05 December 2016)</h3>
                <div class="des">
                    <div class="update-content">
                        <p class="title">
                            <strong>Features - Update slzexploore-core plugin and theme.</strong>
                        </p>
                        <ul>
                            <li>Fixed: Accommodation Booking</li>
                            <li>Fixed: Booking with contact form</li>
                            <li>Fixed: Ribon css</li>
                        </ul>
                    </div>
                </div>
            </dd>
            <dd>
                <h3>Version 3.0 (01 December 2016)</h3>
                <div class="des">
                    <div class="update-content">
                        <p class="title">
                            <strong>Features - Update slzexploore-core plugin and theme.</strong>
                        </p>
                        <ul>
                            <li>Added: SLZ Map Location shortcode - Display location of Accommodation/Tour/Car/Cruise in
                                Google Map.
                            </li>
                            <li>Added: Social in footer and header in Theme Options.</li>
                            <li>Added: Theme Options > Booking Setting - Booking method, using current booking or
                                contact form.
                            </li>
                            <li>Added: Theme Options > Booking Setting - Redirect page when booking finish (No support
                                for booking by Woocommerce)
                            </li>
                            <li>Added: Set label (Hot, New, ... ) in menu.</li>
                            <li>Added: Show ribbon ( Featured, New, ... ) in Accommodation/Tour/Car/Cruise blocks</li>
                            <li>Improved: Date Frequency - Multi select day of week, day of month in Tour and Cruise.
                            </li>
                            <li>Improved: SLZ Image Gallery shortcode</li>
                            <li>Updated: Hidden Cabin Type in Cruise Booking Form when Cabin Type is blank.</li>
                            <li>Fixed: Filter in SLZ Tour Carousel shortcode</li>
                            <li>Fixed: Tranlate title widgets with WPML plugin.</li>
                        </ul>
                    </div>
                </div>
            </dd>
            <dd>
                <h3>Version 2.3 (25 November 2016)</h3>
                <div class="des">
                    <div class="update-content">
                        <p class="title">
                            <strong>Features - Update slzexploore-core plugin and theme.</strong>
                        </p>
                        <ul>
                            <li>Updated: Visual Composer ver 5.0.1.</li>
                        </ul>
                    </div>
                </div>
            </dd>
            <dd>
                <h3>Version 2.2 (21 Septemper 2016)</h3>
                <div class="des">
                    <div class="update-content">
                        <p class="title">
                            <strong>Features - Update slzexploore-core plugin and theme.</strong>
                        </p>
                        <ul>
                            <li>Added: Filter by featured post in Accommodation Grid, Car Grid, Cruise Grid and Tour
                                Grid shortcodes.
                            </li>
                            <li>Added: Tour date by season in Tour and Cruise posts.</li>
                            <li>Added: Infant field in Accommodation, Tour, Car, Cruise.</li>
                            <li>Added: Sort Tour, Cruise by departure date.</li>
                            <li>Added: Option price by person in Accommodation, Cruise.</li>
                            <li>Added: Include email of hotel to mail booking.</li>
                            <li>Added: Theme options - Disable Accommodation, Car, Cruise and Tour by status.</li>
                            <li>Added: Theme options > Page Title Setting - Setting breadcrumb for custom posttypes (
                                Accommodation, Car, Cruise, Team, Tour ).
                            </li>
                            <li>Added: Fixed sidebar when scroll.</li>
                            <li>Improved: Confirm email when booking.</li>
                            <li>Updated: Change currency sign in booking form when change setting in theme option.</li>
                            <li>Updated: Check booking room by schedule.</li>
                            <li>Updated: Document.</li>
                            <li>Updated: pot file.</li>
                            <li>Updated: WPBakery Visual Composer version 4.12.1</li>
                        </ul>
                    </div>
                </div>
            </dd>
            <dd>
                <h3>Version 2.1 (01 Septemper 2016)</h3>
                <div class="des">
                    <div class="update-content">
                        <p class="title">
                            <strong>Features - Update slzexploore-core plugin and theme.</strong>
                        </p>
                        <ul>
                            <li>Added: Support Deposit when booking tour, accommodation, car, cruise.</li>
                            <li>Updated: Social share in tour, car, cruise detail pages.</li>
                            <li>Updated: Setting API key for google map in admin.</li>
                            <li>Updated: pot file.</li>
                        </ul>
                    </div>
                </div>
            </dd>
            <dd>
                <h3>Version 2.0 (26 August 2016)</h3>
                <div class="des">
                    <div class="update-content">
                        <p class="title">
                            <strong>Features - Update slzexploore-core plugin and theme.</strong>
                        </p>
                        <ul>
                            <li>Added: Support for RTL.</li>
                            <li>Added: Setting sidebar for tour, accommodation, car, cruise archive pages.</li>
                            <li>Added: Setting permalink for custom postypes ( tour,accommodation, car, cruise, team
                                ).
                            </li>
                            <li>Updated: Social share in tour, car, cruise lists.</li>
                            <li>Updated: pot file.</li>
                            <li>Improved: Booking tour, accommodation, car, cruise by add to cart of Woocommerce.</li>
                            <li>Improved: Document.</li>
                            <li>Fixed: Css for Woocommerce shortcodes.</li>
                        </ul>
                    </div>
                </div>
            </dd>
            <dd>
                <h3>Version 1.1 (11 August 2016)</h3>
                <div class="des">
                    <div class="update-content">
                        <p class="title">
                            <strong>Features - Update slzexploore-core plugin and theme.</strong>
                        </p>
                        <ul>
                            <li>Added: Theme options > General> Map google API Key.</li>
                            <li>Updated: Social share in post detail.</li>
                            <li>Updated: pot file.</li>
                            <li>Fixed: Booking tour, accommodation, car, cruise.</li>
                            <li>Fixed: Search tour, accommodation, car, cruise.</li>
                            <li>Fixed: Woocommerce style.</li>
                        </ul>
                    </div>
                </div>
            </dd>
            <dd>
                <h3><?php esc_html_e( 'Version 1.0', 'exploore' ) ?></h3>
                <div class="des">
                    <div class="update-content">
                        <ul>
                            <li><?php esc_html_e( 'Initital', 'exploore' ) ?></li>
                        </ul>
                    </div>
                </div>
            </dd>
        </dl>
    </div>
</div>
